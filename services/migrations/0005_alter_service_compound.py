# Generated by Django 4.1.3 on 2022-11-28 13:04

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('services', '0004_alter_service_price'),
    ]

    operations = [
        migrations.AlterField(
            model_name='service',
            name='compound',
            field=models.TextField(blank=True, verbose_name='Описание'),
        ),
    ]
