from django.db import models
from django.db.models import Q
from django.core.exceptions import ValidationError
from django.urls import reverse
import datetime


class Order(models.Model):
    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=40, verbose_name='ФИО')
    phonenumber = models.CharField(max_length=12, verbose_name='Номер телефона')
    date = models.DateField(verbose_name='Дата')
    time = models.TimeField(verbose_name='Время')

    def clean(self):
        super(Order, self).clean()
        timeHour = Order.objects.filter(Q(time__hour__icontains=self.time.strftime(format='%H')) & Q(date__day__icontains=self.date.strftime(format='%d')))
        if timeHour:
            raise ValidationError("Извините, это время забронировано, выберите другое!")
        if self.date < datetime.date.today():
            raise ValidationError("Нельзя выбрать прошедшие даты.")
        if self.time.hour < 8 or self.time.hour > 22:
            raise ValidationError("Время работы: 8:00 - 22:00.")

    def __str__(self):
        return self.name[:50]
    def get_absolute_url(self):
        return reverse('home')
