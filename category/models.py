from django.db import models

class Category(models.Model):
    id = models.AutoField(primary_key=True)
    icon = models.ImageField(verbose_name='Иконка', upload_to='img/%Y-%m-%d/', blank=True)
    title = models.CharField(max_length=55, verbose_name='Название')


    def photo_url(self):
        if self.icon and hasattr(self.icon, 'url'):
            return self.icon.url

    def __str__(self):
        return self.title[:50]
