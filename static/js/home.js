$(function() { 
    $(document).on("click", "#delete", function(){
        var items = [];
        $("#item:checked").each(function(i, item) {
            items.push($(item).attr("value"))
        });
        if (items != ""){
            $.ajax({
                type: "POST",
                url: "services/order/deleteorder/",
                data: {
                    'items' : items,
                    'csrfmiddlewaretoken': $('[name=csrfmiddlewaretoken]').val()
                },
                success: function(data){
                    for (let i = 0; i < items.length; i++){
                        $("#item-" + items[i]).remove();
                    }
                }
                })
        } else {
            document.querySelector('.modal').classList.add('shown')
            document.querySelector('.ok-button').addEventListener("click", function(){
                document.querySelector('.modal').classList.remove('shown')
            })
        }
        
    });
});