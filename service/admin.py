from django.contrib import admin
from . import models

class CommentInline(admin.TabularInline):
    model = models.Comment


admin.site.register(models.Comment)