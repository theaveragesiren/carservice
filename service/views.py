from django.shortcuts import redirect
from django.views.generic import DetailView
from django.contrib.auth.mixins import PermissionRequiredMixin, LoginRequiredMixin
from django.views.generic.edit import UpdateView
from django.urls import reverse_lazy, reverse
from . import models


class ServiceDetailView(DetailView):
    template_name = 'service-detail.html'
    model = models.Service


class ServiceUpdateView(LoginRequiredMixin, PermissionRequiredMixin, UpdateView):
    permission_required = 'user.is_superuser'
    model = models.Service
    template_name = 'service-change.html'
    fields = ['category', 'icon' , 'title', 'price', 'compound']
    success_url = reverse_lazy('service')
    
    def get_success_url(self):
        return reverse('service-detail', kwargs={'pk': self.object.pk})

def deleteService(request):
    models.Service.objects.filter(id=request.POST.get("id")).delete()
    return redirect(reverse_lazy('service'))

def comment(request):
    serivce = models.Service.objects.get(id=request.POST.get('service'))
    comment_model = models.Comment()
    if request.method == "POST":
        comment_model.author = request.user
        comment_model.services = serivce
        comment_model.comment = request.POST.get("comment")
        comment_model.save()
    return redirect('service-detail',  pk=serivce.id)

def deleteComment(request):
    models.Comment.objects.filter(id=request.POST.get("id")).delete()
    return redirect('service-detail',  pk=request.POST.get('service'))