from django.db import models
from django.conf import settings
from services.models import Service



class Comment(models.Model):
    id = models.AutoField(primary_key=True)
    comment = models.CharField(max_length=140)
    services = models.ForeignKey(Service, on_delete=models.CASCADE, related_name='comments')
    date = models.DateTimeField(auto_now_add=True)
    author = models.ForeignKey(settings.AUTH_USER_MODEL,on_delete=models.CASCADE)
    
    def __str__(self):
        return self.comment




