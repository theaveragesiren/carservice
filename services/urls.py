from django.urls import path, include
from . import views

urlpatterns = [ 
    path('rendercategory/',views.category),
    path('services/',views.ServicesPageView.as_view(), name="service"),
    path('services/', include('service.urls')),
    path('listcategory/', include('category.urls')),
    path('newservice/', views.ServiceCreateView.as_view(), name='service-new'),
    path('', views.HomePageView.as_view(), name='home'),
] 
