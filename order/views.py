from django.shortcuts import render
from django.views.generic.edit import CreateView, UpdateView
from django.contrib.auth.mixins import LoginRequiredMixin
from . import models
from . import forms


class OrderCreateView(LoginRequiredMixin, CreateView):
    form_class = forms.OrderCreateForm
    template_name = 'order-new.html'
    login_url = 'login'

class OrderChangeView(LoginRequiredMixin, UpdateView):
    model = models.Order
    form_class = forms.OrderCreateForm
    template_name = 'order-change.html'
    login_url = 'login'

def deleteOrder(request):
    if request.POST.getlist('items[]'):
        models.Order.objects.filter(id__in=request.POST.getlist('items[]')).delete()
    return render(request, 'home.html')

