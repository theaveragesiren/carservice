
$(function() { //Функция при вызове декйствиях в блоке филтров и поиска
    $(document).on("click", ".category", function(event){
        $.ajax({
            type: "GET",
            url: "/rendercategory", 
            data: { 
                'category' : event.target.getAttribute('value'),
            },
            dataType: 'json', 
            success: function (data) {
                CategoryList = ""
                list = JSON.parse(data)
                for (service in list) {
                    CategoryList += `<li class = "list-group-item list-group-item-action shadow">
                                        <div class="card-img-background">
                                            <img src="${list[service].fields.icon == '' ? `/static/img/4054617.png`:`/media/${list[service].fields.icon}`}" width="142" height="132">
                                        </div>
                                        <div class="card-content text-center">
                                            <p><a class="stretched-link text-dark" href="${list[service].pk}/servicedetail/">${list[service].fields.title}</a></p>
                                        </div>
                                        <div class="text-center">
                                            <p class="h3 text-warning ">${list[service].fields.price} ₽</p>  
                                        </div>
                                        
                                    </li>`
                }
                SelectCategory = `<div class="ml-5 mt-5">
                                    <h4 class="font-weight-bold">Выберите услугу</h4>
                                </div>
                                <div class="container-fluid mt-5">
                                    <ul class="cards ">
                                    ${CategoryList}
                                    </ul>
                                </div>`
                document.getElementById('service').innerHTML = SelectCategory
            }
        })
    })
})

