from django.db import models
from category.models import Category


class Service(models.Model):
    id = models.AutoField(primary_key=True)
    category = models.ForeignKey(Category, on_delete=models.CASCADE, verbose_name='Категория', related_name='categorys')
    icon = models.ImageField(verbose_name='Иконка', upload_to='img/%Y-%m-%d/', blank=True)
    title = models.CharField(max_length=255, verbose_name='Название')
    price = models.IntegerField(verbose_name='Цена')
    compound = models.TextField(max_length=1000, verbose_name='Описание', blank=True)
    def photo_url(self):
        if self.icon and hasattr(self.icon, 'url'):
            return self.icon.url
    def __str__(self):
        return self.title[:50]


