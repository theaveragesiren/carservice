from django.urls import path, include
from . import views

urlpatterns = [ 

    path('<int:pk>/servicedetail/',views.ServiceDetailView.as_view(), name="service-detail"),
    path('order/', include('order.urls')),
    path('comment/', views.comment, name='comment'),
    path('deletecomment/', views.deleteComment, name='delete-comment'),
    path('<int:pk>/changeservice/', views.ServiceUpdateView.as_view(), name='service-change'),
    path('deleteservice/', views.deleteService, name='deleteservice')


]

