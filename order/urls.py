from django.urls import path
from . import views

urlpatterns = [
    path('ordernew/',views.OrderCreateView.as_view(), name="order"),
    path('<int:pk>/orderchange/',views.OrderChangeView.as_view(), name="order_change"),
    path('deleteorder/', views.deleteOrder,),

]