from django.conf import settings
from django.conf.urls.static import static
from django.urls import path
from . import views

urlpatterns = [ 
    path('newcategory/', views.CategoryCreateView.as_view(), name='category-new'),
    path('<int:pk>/changecategory/', views.CategoryChangeView.as_view(), name='category-change'),
    path('listcategory/', views.CategoryPageView.as_view(), name='category-list'),
    path('deletecategory/', views.deleteCategory, name='deletecategory'),
] 