from django import forms
from . import models

class OrderCreateForm(forms.ModelForm):
    class Meta:
        model = models.Order
        fields = ['name', 'phonenumber', 'date', 'time' ]

        widgets = {
            'date': forms.DateInput(format='%Y-%m-%d', attrs={'type': 'date'}),
            'time': forms.TimeInput(format='%H:%M', attrs={'type': 'time'}),
        }
