from django.shortcuts import redirect
from django.contrib.auth.mixins import PermissionRequiredMixin, LoginRequiredMixin
from django.views.generic import ListView

from django.urls import reverse_lazy
from django.views.generic.edit import CreateView, UpdateView
from . import models

class CategoryPageView(LoginRequiredMixin, ListView):
    template_name="category-list.html"
    model = models.Category
    login_url = 'login'

class CategoryCreateView(LoginRequiredMixin, PermissionRequiredMixin, CreateView):
    permission_required = 'user.is_superuser'
    model = models.Category
    template_name = 'category-new.html'
    fields = ['title', 'icon']
    success_url = reverse_lazy('category-list')
    login_url = 'login'

class CategoryChangeView(LoginRequiredMixin, PermissionRequiredMixin, UpdateView):
    permission_required = 'user.is_superuser'
    model = models.Category
    template_name = 'category-change.html'
    fields = ['title', 'icon']
    success_url = reverse_lazy('category-list')
    login_url = 'login'


def deleteCategory(request):
    models.Category.objects.filter(id=request.POST.get("id")).delete()
    return redirect('category-list')