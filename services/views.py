from django.contrib.auth.mixins import PermissionRequiredMixin, LoginRequiredMixin
from django.http import JsonResponse
from django.core import serializers
from django.views.generic import ListView
from order.models import Order
from django.urls import reverse_lazy
from django.views.generic.edit import CreateView
from . import models


class ServicesPageView(ListView):
    template_name="service.html"
    model = models.Service
    def get_context_data(self, **kwargs):
        context = super(ServicesPageView, self).get_context_data(**kwargs)
        context['categorys'] = models.Category.objects.all()
        return context

class HomePageView(ListView):
    template_name = "home.html"
    model = Order

def category(request):
    if request.method == "GET":
        service = models.Service.objects.filter(category__id__iregex=request.GET['category'])
        return JsonResponse(serializers.serialize('json', service), safe=False)


class ServiceCreateView(LoginRequiredMixin, PermissionRequiredMixin ,CreateView):
    permission_required = 'user.is_superuser'
    model = models.Service
    template_name = 'service-new.html'
    fields = ['category', 'icon' , 'title', 'price', 'compound']
    success_url = reverse_lazy('service')
    login_url = 'login'

